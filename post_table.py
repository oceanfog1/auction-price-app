import boto3
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError

dynamo_client = boto3.client('dynamodb')
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('post')


def get_item():
    return dynamo_client.scan(
        TableName='post'
    )


def select_all(self):
    r = self.table.scan()
    return r


def list_table():
    response = dynamo_client.list_tables()
    return response


'''
def insert_row(table_name, row):
    r = table.put_item(Item=row)
    print(r)

insert_row('post', b)
'''


def select_row(table_name, row, q):
    try:
        r = table.get_item(Key=q)
    except ClientError as e:
        print(e.response['Error']['Message'])
    return r['Item']


def select_all():
    r = table.scan()
    return list(r)


print(type(select_all()))
